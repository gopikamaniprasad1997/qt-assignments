#include "urlform.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    UrlForm w;
    w.show();
    return a.exec();
}
