#include "urlform.h"
#include "ui_urlform.h"

UrlForm::UrlForm(QWidget *parent)
    : QDialog(parent)

{
    protocol = new QComboBox(this);
       protocol->addItem("http");
       protocol->addItem("ftp");
       protocol->addItem("tcp");
       protocol->addItem("smtp");
       protocol->addItem("https");

       hostname = new QLineEdit(this);
       portNumber = new QSpinBox(this);
       portNumber->setRange(0,1023);
       urlButton = new QPushButton("generate",this);
       clearButton = new QPushButton("clear",this);
       url = new QLineEdit(this);

       layout = new QFormLayout(this);
       layout->addRow(tr("Protocol :"), protocol);
       layout->addRow(tr("Hostname :"), hostname);
       layout->addRow(tr("Port Number :"), portNumber);
       layout->addRow(clearButton, urlButton);
       layout->addRow(tr("URL :"), url);
       layout->setVerticalSpacing(12);
       setLayout(layout);
       connect(urlButton,SIGNAL(clicked()),this,SLOT(OnURLButtonClicked()));
       connect(clearButton,SIGNAL(clicked()),this,SLOT(ClearAll()));

}
void UrlForm::ClearAll()
{
    hostname->clear();
    portNumber->clear();
    url->clear();

}
void UrlForm::OnURLButtonClicked()
{
    QString host1 = hostname->text();
    QString protocolentered = protocol->currentText();
    QString portnumber = QString::number(portNumber->value());
    QString resulturl =  protocolentered + "://" + host1 + ":" + portnumber;
    url->setText(resulturl);
}
UrlForm::~UrlForm()
{

}

