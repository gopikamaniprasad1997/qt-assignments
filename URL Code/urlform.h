#ifndef URLFORM_H
#define URLFORM_H

#include <QDialog>
#include <QComboBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QPushButton>
#include <QFormLayout>



class UrlForm : public QDialog
{
    Q_OBJECT

public:
    UrlForm(QWidget *parent = nullptr);
    ~UrlForm();
public slots:
    void OnURLButtonClicked();
    void ClearAll();
private:
    QComboBox *protocol;
    QLineEdit *hostname;
    QSpinBox *portNumber;
    QPushButton *urlButton;
    QPushButton *clearButton;
    QLineEdit *url;
    QFormLayout *layout;
};
#endif // URLFORM_H
