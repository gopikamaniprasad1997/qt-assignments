#include "combobox.h"
#include "ui_combobox.h"
#include<QDebug>
#include<QString>
ComboBox::ComboBox(QWidget *parent)
    : QWidget(parent)

{
    first = new QLineEdit(this);
    second = new QLineEdit(this);
    result = new QLineEdit(this);
    clearButton = new QPushButton("clear",this);
    //select = new QPushButton("choose",this);
    combo = new QComboBox(this);
    layout = new QVBoxLayout(this);
    layout->addWidget(first);
    layout->addWidget(second);
    layout->addWidget(combo);
    //layout->addWidget(select);
    layout->addWidget(result);
    layout->addWidget(clearButton);
    combo->addItem("Add",0);
    combo->addItem("Subtract",1);
    combo->addItem("Multiply",2);
    combo->addItem("Divide",3);
    connect(combo,SIGNAL(activated(QString)),this,SLOT(OnChoosingComboButtonOperate(QString)));
    //connect(select,SIGNAL(clicked()),this,SLOT(OnChoosingComboButtonOperate(QString)));
    connect(clearButton,SIGNAL(clicked()),this,SLOT(ClearAll()));
}
void ComboBox::OnChoosingComboButtonOperate(QString string12)
{
    QString str1 = first->text();
    QString str2 = second->text();
    int num1 = str1.toInt();
    int num2 = str2.toInt();
    int res;
    if(string12=="Add")
    {
        res = num1 + num2;
        result->setText(QString::number(res));
        qDebug()<<"Sum is "<<res;
    }
    else if(string12 == "Subtract")
    {
        res = num1 - num2;
        result->setText(QString::number(res));
        qDebug()<<"Difference is "<<res;
    }
    else if(string12 == "Multiply")
    {
        res = num1 * num2;
        result->setText(QString::number(res));
        qDebug()<<"Product is "<<res;
    }
    else if(string12 == "Divide")
    {
        res = num1 / num2;
        result->setText(QString::number(res));
        qDebug()<<"Quotient is "<<res;
    }

}
void ComboBox::ClearAll()
{
    first->clear();
    second->clear();
    result->clear();
    qDebug()<<"Cleared!!";
}
ComboBox::~ComboBox()
{

}

