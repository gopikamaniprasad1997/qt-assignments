#ifndef COMBOBOX_H
#define COMBOBOX_H

#include <QDialog>
#include<QComboBox>
#include<QLineEdit>
#include<QPushButton>
#include<QWidget>
#include<QVBoxLayout>
#include<QString>

class ComboBox : public QWidget
{
    Q_OBJECT

public:
    ComboBox(QWidget *parent = nullptr);
    ~ComboBox();
private slots:
    void OnChoosingComboButtonOperate(QString);
    void ClearAll();
private:
    QComboBox *combo;
    QLineEdit *first;
    QLineEdit *second;
    QLineEdit *result;
    QPushButton *clearButton;
    //QPushButton *select;
    QLayout *layout;
};
#endif // COMBOBOX_H
