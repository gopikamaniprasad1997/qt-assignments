#include "maindialog.h"
#include "ui_maindialog.h"
#include<QString>
#include<QDebug>
MainDialog::MainDialog(QWidget *parent)
    : QDialog(parent)

{
    addButton = new QRadioButton("Add",this);
    SubButton = new QRadioButton("Subtract",this);
    MultButton = new QRadioButton("Multiply",this);
    DivButton = new QRadioButton("Divide",this);
    firstline = new QLineEdit(this);
    secondline =  new QLineEdit(this);
    resultline = new QLineEdit(this);
   // CalcButton = new QPushButton("Test",this);
    ClearButton = new QPushButton("Clear",this);
        layout1 = new QGridLayout(this);
        layout1->addWidget(addButton,0,0);
        layout1->addWidget(SubButton,0,1);
        layout1->addWidget(MultButton,1,0);
        layout1->addWidget(DivButton,1,1);
        layout1->addWidget(firstline,2,0);
        layout1->addWidget(secondline,2,1);
//        layout1->addWidget(CalcButton,3,0);
        layout1->addWidget(ClearButton,3,1);
        layout1->addWidget(resultline,3,0);
        connect(addButton,SIGNAL(clicked()),this,SLOT(ComputeAddition()));
        connect(SubButton,SIGNAL(clicked()),this,SLOT(ComputeSubtraction()));
        connect(MultButton,SIGNAL(clicked()),this,SLOT(ComputeMultiplication()));
        connect(DivButton,SIGNAL(clicked()),this,SLOT(ComputeDivision()));
        connect(ClearButton,SIGNAL(clicked()),this,SLOT(ClearAll()));
}
void MainDialog::ComputeAddition()
{
    QString str1 = firstline->text();
        QString str2 = secondline->text();
        int num1 = str1.toInt();
        int num2 = str2.toInt();
        int res = num1 + num2;
        resultline->setText(QString::number(res));
        qDebug() << "Result of Addition is " << res;

}
void MainDialog::ComputeSubtraction()
{
    QString str1 = firstline->text();
        QString str2 = secondline->text();
        int num1 = str1.toInt();
        int num2 = str2.toInt();
        int res = num1 - num2;
        resultline->setText(QString::number(res));
        qDebug() << "Result of Subtraction is " << res;
}
void MainDialog::ComputeMultiplication()
{
    QString str1 = firstline->text();
        QString str2 = secondline->text();
        int num1 = str1.toInt();
        int num2 = str2.toInt();
        int res = num1 * num2;
        resultline->setText(QString::number(res));
        qDebug() << "Result of Multiplication is " << res;
}
void MainDialog::ComputeDivision()
{
    QString str1 = firstline->text();
        QString str2 = secondline->text();
        int num1 = str1.toInt();
        int num2 = str2.toInt();
        float res = num1 / num2;
        resultline->setText(QString::number(res));
        qDebug() << "Result of Addition is " << res;
}
void MainDialog::ClearAll()
{
    firstline->clear();
    secondline->clear();
    resultline->clear();
    qDebug()<<"Cleared!!";
}
MainDialog::~MainDialog()
{

}

