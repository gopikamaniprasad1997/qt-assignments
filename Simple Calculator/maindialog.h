#ifndef MAINDIALOG_H
#define MAINDIALOG_H

#include<QDialog>
#include<QPushButton>
#include<QRadioButton>
#include<QGridLayout>
#include<QDebug>
#include<QVBoxLayout>
#include<QLineEdit>
class MainDialog : public QDialog
{
    Q_OBJECT

public:
    MainDialog(QWidget *parent = nullptr);
    ~MainDialog();
public slots:
    void ComputeAddition();
    void ComputeSubtraction();
    void ComputeMultiplication();
    void ComputeDivision();
    void ClearAll();
private:
    //QPushButton *CalcButton;
    QPushButton *ClearButton;
    QRadioButton *addButton;
    QRadioButton *SubButton;
    QRadioButton *MultButton;
    QRadioButton *DivButton;
    QGridLayout *layout1;
    //QLayout *layout2;
    QLineEdit *firstline;
    QLineEdit *secondline;
    QLineEdit *resultline;

};
#endif // MAINDIALOG_H
