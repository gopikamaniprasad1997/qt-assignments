#ifndef SIMPLEUI_H
#define SIMPLEUI_H

#include <QWidget>
#include<QPushButton>
#include<QRadioButton>
#include<QLineEdit>
#include<QLabel>
#include<QHBoxLayout>
#include<QGridLayout>
class SimpleUI : public QWidget
{
    Q_OBJECT

public:
    SimpleUI(QWidget *parent = nullptr);
    ~SimpleUI();
public slots:
    void ImplementOnClickingButtonForLength();
    void ImplementOnClickingButtonForCopy();
    void ImplementOnClickingButtonForConcatenation();
    void ImplementOnClickingButtonForCompare();
    void ClearAll();
private:
    //Ui::SimpleUI *ui;
    QLineEdit *line1;
    QLineEdit *line2;
    QLineEdit *result;
    QGridLayout *layout;
    QRadioButton *LengthButton;
    QRadioButton *copyButton;
    QRadioButton *ConcatenateButton;
    QRadioButton *CompareButton;
    QLabel *label;
    QPushButton *clearButton;
};
#endif // SIMPLEUI_H
