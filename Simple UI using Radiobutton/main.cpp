#include "simpleui.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    SimpleUI w;
    w.show();
    return a.exec();
}
