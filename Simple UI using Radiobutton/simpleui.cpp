#include "simpleui.h"
#include "ui_simpleui.h"
#include<QDebug>
#include<QString>

SimpleUI::SimpleUI(QWidget *parent)
    : QWidget(parent)

{

    line1 = new QLineEdit(this);
    line2 = new QLineEdit(this);

    LengthButton = new QRadioButton("Length",this);
    copyButton = new QRadioButton("Copy String",this);
    ConcatenateButton =  new QRadioButton("Concatenate",this);
    CompareButton = new QRadioButton("Compare Strings",this);

    result = new QLineEdit(this);
    clearButton =  new QPushButton("Clear Data",this);
    layout = new QGridLayout(this);
   // label->setText("Line 1");
    layout->addWidget(line1,0,0);
    //label->setText("Line 2");
    layout->addWidget(line2,0,1);
    //label->setText("Options");
    layout->addWidget(LengthButton,1,0);
    layout->addWidget(copyButton,1,1);
    layout->addWidget(ConcatenateButton,2,0);
    layout->addWidget(CompareButton,2,1);
    //label->setText("Result");
    layout->addWidget(result,3,0);
    layout->addWidget(clearButton,3,1);
    connect(LengthButton,SIGNAL(clicked()),this,SLOT(ImplementOnClickingButtonForLength()));
    connect(copyButton,SIGNAL(clicked()),this,SLOT(ImplementOnClickingButtonForCopy()));
    connect(ConcatenateButton,SIGNAL(clicked()),this,SLOT(ImplementOnClickingButtonForConcatenation()));
    connect(CompareButton,SIGNAL(clicked()),this,SLOT(ImplementOnClickingButtonForCompare()));
    connect(clearButton,SIGNAL(clicked()),SLOT(ClearAll()));


}
void SimpleUI::ImplementOnClickingButtonForLength()
{
    QString str1 = line1->text();
    int count1 = str1.count();
    result->setText(QString::number(count1));
    qDebug()<<"The length of line 1 string is "<<count1;
}
void SimpleUI::ImplementOnClickingButtonForCopy()
{
    QString str1 = line1->text();
    result->setText(str1);
    qDebug()<<"The string copied ";
}
void SimpleUI::ImplementOnClickingButtonForCompare()
{
    QString str1 = line1->text();
    QString str2 = line2->text();
    int x = QString::compare(str1,str2,Qt::CaseSensitive);
    if(x==0)
    {
        result->setText("Strings are equal");

    }else if(x>0)
    {
        result->setText("String 1 greater than String 2");
    }else
    {
        result->setText("String1 less than String 2");
    }
    qDebug()<<"Comparison of Strings Done ";
}
void SimpleUI::ImplementOnClickingButtonForConcatenation()
{
    QString str1 = line1->text();
    QString str2 = line2->text();
    QString str3 = str1 + str2;
    result->setText(str3);
    qDebug()<<"Concatenated String is "<<str3;
}
void SimpleUI::ClearAll()
{
    line1->clear();
    line2->clear();
    result->clear();
}
SimpleUI::~SimpleUI()
{

}

