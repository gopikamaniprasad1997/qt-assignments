#ifndef SIMPLEUI_H
#define SIMPLEUI_H

#include <QWidget>
#include<QPushButton>
#include<QLineEdit>
#include<QLabel>
#include<QHBoxLayout>
#include<QGridLayout>
#include<QComboBox>
class SimpleUI : public QWidget
{
    Q_OBJECT

public:
    SimpleUI(QWidget *parent = nullptr);
    ~SimpleUI();
private slots:
    void ImplementOnClickingCombobutton(QString);
    void ClearAll();
private:
    //Ui::SimpleUI *ui;
    QLineEdit *line1;
    QLineEdit *line2;
    QLineEdit *result;
    //QGridLayout *layout;
    QLayout *layout;
    QComboBox *combo;
    QLabel *label;
    QPushButton *clearButton;
};
#endif // SIMPLEUI_H
