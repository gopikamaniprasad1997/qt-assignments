#include "simpleui.h"
#include "ui_simpleui.h"
#include<QDebug>
#include<QString>

SimpleUI::SimpleUI(QWidget *parent)
    : QWidget(parent)

{

    line1 = new QLineEdit(this);
    line2 = new QLineEdit(this);

    combo = new QComboBox(this);
    result = new QLineEdit(this);
    clearButton =  new QPushButton("Clear Data",this);
    layout = new QHBoxLayout(this);
    layout->addWidget(line1);
    layout->addWidget(line2);
    layout->addWidget(combo);
    layout->addWidget(result);
    layout->addWidget(clearButton);
    combo->addItem("Length",0);
    combo->addItem("Copy",1);
    combo->addItem("Concatenate",2);
    combo->addItem("Compare",3);
    connect(combo,SIGNAL(activated(QString)),this,SLOT(ImplementOnClickingCombobutton(QString)));
   connect(clearButton,SIGNAL(clicked()),this,SLOT(ClearAll()));


}
void SimpleUI::ImplementOnClickingCombobutton(QString str)
{
    QString str1 = line1->text();
    int count1 = str1.count();
    QString str2 = line2->text();
    if(str == "Length")
    {
        result->setText(QString::number(count1));
        qDebug()<<"The length of line 1 string is "<<count1;
    }
    if(str == "Copy")
    {
        QString str1 = line1->text();
        result->setText(str1);
        qDebug()<<"The string copied ";
    }
    if(str == "Compare")
    {
        int x = QString::compare(str1,str2,Qt::CaseSensitive);
        if(x==0)
        {
            result->setText("Strings are equal");

        }else if(x>0)
        {
            result->setText("String 1 greater than String 2");
        }else
        {
            result->setText("String1 less than String 2");
        }
        qDebug()<<"Comparison of Strings Done ";
    }
    if(str == "Concatenate")
    {
        QString str3 = str1 + str2;
        result->setText(str3);
        qDebug()<<"Concatenated String is "<<str3;
    }


}
void SimpleUI::ClearAll()
{
    line1->clear();
    line2->clear();
    result->clear();
}
SimpleUI::~SimpleUI()
{

}

