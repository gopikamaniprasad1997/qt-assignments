#ifndef COUNTDOWN_H
#define COUNTDOWN_H

//#include <QMainWindow>
#include<QWidget>
#include<QPushButton>
#include<QTimer>
#include<QSpinBox>
#include<QLCDNumber>

class CountDown : public QWidget
{
    Q_OBJECT

public:
    CountDown(QWidget *parent = nullptr);
    ~CountDown();


public slots:
    void onButtonStarted();
    void onButtonStopped();
    void Update();
private:
    int hcount;
    int scount;
    int mcount;
    QLCDNumber *number1;
    QLCDNumber *number2;
    QLCDNumber *number3;
    QSpinBox *box1;
    QSpinBox *box2;
    QSpinBox *box3;
    QTimer *updatetimer;
    QPushButton *start;
    QPushButton *stop;

};
#endif // COUNTDOWN_H
