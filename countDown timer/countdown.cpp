#include "countdown.h"
#include "ui_countdown.h"

CountDown::CountDown(QWidget *parent)
    : QWidget(parent)

{
    number1 = new QLCDNumber(this);
    number1->setGeometry(100,100,80,30);
    number1->setFixedSize(60,60);

    number2 = new QLCDNumber(this);
    number2->setGeometry(200,100,80,30);
    number2->setFixedSize(60,60);

    number3 = new QLCDNumber(this);
    number3->setGeometry(300,100,80,30);
    number3->setFixedSize(60,60);

    box1 = new QSpinBox(this);
    box1->setGeometry(100,200,80,30);
    box1->setFixedSize(60,60);

    box2 = new QSpinBox(this);
    box2->setGeometry(200,200,80,30);
    box2->setFixedSize(60,60);

    box3 = new QSpinBox(this);
    box3->setGeometry(300,200,80,30);
    box3->setFixedSize(60,60);

    updatetimer =  new QTimer(this);

    start =  new QPushButton(this);
    stop = new QPushButton(this);

    connect(start, SIGNAL(clicked()), this, SLOT(onButtonStarted()));
    connect(stop, SIGNAL(clicked()), this, SLOT(onButtonStopped()));
    connect(updatetimer, SIGNAL(timeout()), this, SLOT(Update()));


}
void CountDown::onButtonStarted()
{
    updatetimer->start(1000);
}
void CountDown::Update()
{
    scount--;
    if(scount<0)
    {
        scount = 59;
        mcount--;
    }
    if(mcount<0)
    {
        mcount = 59;
        hcount--;
    }
    if(hcount<0)
    {
        hcount = 24;
    }
    number1->display(hcount);
    number2->display(mcount);
    number3->display(scount);

}
void CountDown::onButtonStopped()
{
    hcount = box1->value();
    mcount = box2->value();
    scount = box3->value();
    number1->display(hcount);
    number2->display(mcount);
    number3->display(scount);
}
CountDown::~CountDown()
{

}

