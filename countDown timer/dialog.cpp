#include "dialog.h"

Dialog::Dialog(QWidget *parent)
    : QDialog(parent),time(""),setTime()
{
    outerLayout = new QVBoxLayout(this);
    innerLayout = new QHBoxLayout(this);
    labelinnerLayout = new QHBoxLayout(this);
    lcdNumber = new QLCDNumber(this);
    lcdNumber->setMinimumSize(300,150);
    lcdNumber->setDigitCount(8);
    hourLabel = new QLabel("hrs",this);
    minLabel = new QLabel("mins",this);
    secLabel = new QLabel("secs",this);
    hours = new QSpinBox(this);
    hours->setRange(0,24);
    mins = new QSpinBox(this);
    mins->setRange(0,60);
    secs = new QSpinBox(this);
    secs->setRange(0,60);

    timer = new QTimer(this);
    timer->setInterval(1000);
    connect(timer, SIGNAL(timeout()),this, SLOT(onTimeOut()));

    setTimeButton = new QPushButton("set time",this);
    startButton = new QPushButton("start",this);
    stopButton = new QPushButton("stop", this);

    outerLayout->addWidget(lcdNumber);
    labelinnerLayout->addWidget(hourLabel);
    labelinnerLayout->addWidget(minLabel);
    labelinnerLayout->addWidget(secLabel);
    innerLayout->addWidget(hours);
    innerLayout->addWidget(mins);
    innerLayout->addWidget(secs);
    outerLayout->addItem(labelinnerLayout);
    outerLayout->addItem(innerLayout);
    outerLayout->addWidget(setTimeButton);
    outerLayout->addWidget(startButton);
    outerLayout->addWidget(stopButton);
    setLayout(outerLayout);

    connect(setTimeButton, SIGNAL(clicked()), this, SLOT(onClickSetTimeButton()));
    connect(startButton, SIGNAL(clicked()), this, SLOT( onClickStartButton()));
    connect(stopButton, SIGNAL(clicked()), this, SLOT(onClickStopButton()));
}

void Dialog :: onClickStartButton()
{
    timer->start();
}

void Dialog :: onClickStopButton()
{
    timer->stop();
}

void Dialog :: onClickSetTimeButton()
{
    int h = hours->value();
    int m = mins->value();
    int s = secs->value();
    setTime.setHMS(h,m,s);
    time = setTime.toString("hh:mm:ss");
    qDebug() << time;
    lcdNumber->display(time);
}

void Dialog :: onTimeOut()
{
    setTime = setTime.addSecs(-1);
    time = setTime.toString("hh:mm:ss");
    qDebug() << time;
    lcdNumber->display(time);
    if(time == "00:00:00")
    {
        timer->stop();
        QMessageBox msg;
        msg.setText("Time is up");
        QAbstractButton* pButtonSnooze = msg.addButton(tr("Snooze"), QMessageBox::YesRole);
        msg.addButton(tr("Dismiss"), QMessageBox::NoRole);
        msg.exec();
        if (msg.clickedButton()==pButtonSnooze) {
            timer->start();
            int h = hours->value();
            int m = mins->value();
            int s = secs->value();
            setTime.setHMS(h,m,s);
            time = setTime.toString("hh:mm:ss");
            qDebug() << time;
            lcdNumber->display(time);
        }

       else
        {
            timer->stop();
        }

    }
}

Dialog::~Dialog()
{
}

