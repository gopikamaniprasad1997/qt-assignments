#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QLCDNumber>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSpinBox>
#include <QPushButton>
#include <QLabel>
#include <QTime>
#include <QDebug>
#include <QTimer>
#include <QMessageBox>

class Dialog : public QDialog
{
    Q_OBJECT

public:
    Dialog(QWidget *parent = nullptr);
    ~Dialog();

public slots:
    void onClickStartButton();
    void onClickStopButton();
    void onClickSetTimeButton();
    void onTimeOut();

private:
    QVBoxLayout *outerLayout;
    QHBoxLayout *innerLayout;
    QHBoxLayout *labelinnerLayout;
    QLCDNumber *lcdNumber;
    QLabel *hourLabel;
    QLabel *minLabel;
    QLabel *secLabel;
    QSpinBox *hours;
    QSpinBox *mins;
    QSpinBox *secs;
    QPushButton *setTimeButton;
    QPushButton *startButton;
    QPushButton *stopButton;
    QTimer *timer;
    QString time;
    QTime setTime;
};
#endif // DIALOG_H
