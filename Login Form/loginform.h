#ifndef LOGINFORM_H
#define LOGINFORM_H
#include"seconddialog.h"
#include <QDialog>
#include<QMainWindow>
#include<QLineEdit>
#include<QPushButton>
#include<QVBoxLayout>
#include<QGroupBox>
#include<QDebug>
#include<QMessageBox>
QT_BEGIN_NAMESPACE
namespace Ui { class LoginForm; }
QT_END_NAMESPACE

class LoginForm : public QDialog
{
    Q_OBJECT

public:
    LoginForm(QWidget *parent = nullptr);
    ~LoginForm();

private slots:
    //void on_pushButton_clicked();

    void on_loginButton_clicked();

private:
    Ui::LoginForm *ui;
    SecondDialog * secdialog;
};
#endif // LOGINFORM_H
